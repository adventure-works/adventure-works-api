const db = require("../config/database");

const Product = function (product) {
    this.ProductID = product.ProductID;
    this.Name = product.Name;
    this.ProductNumber = product.ProductNumber;
    this.MakeFlag = product.MakeFlag;
    this.FinishedGoodsFlag = product.FinishedGoodsFlag;
    this.Color = product.Color;
    this.SafetyStockLevel = product.SafetyStockLevel
    this.ReorderPoint = product.ReorderPoint;
    this.StandardCost = product.StandardCost;
    this.ListPrice = product.ListPrice;
    this.Size = product.Size;
    this.SizeUnitMeasureCode = product.SizeUnitMeasureCode;
    this.WeightUnitMeasureCode = product.WeightUnitMeasureCode;
    this.Weight = product.Weight;
    this.DaysToManufacture = product.DaysToManufacture;
    this.ProductLine = product.ProductLine;
    this.Class = product.Class;
    this.Style = product.Style;
    this.ProductSubcategoryID = product.ProductSubcategoryID;
    this.ProductModelID = product.ProductModelID;
    this.SellStartDate = product.SellStartDate;
    this.SellEndDate = product.SellEndDate;
    this.DiscontinuedDate = product.DiscontinuedDate;
    this.rowguid = product.rowguid;
    this.ModifiedDate = product.ModifiedDate;
}

Product.create = (newProduct, result) => {

    db.query("INSERT INTO product SET ?", newProduct, (err, res) => {
        if (err) {
            result(err, null);
            return;
        }

        result(null, { id: res.insertId, ...newProduct });
    });
};

Product.getAll = result => {

    let query = "SELECT p.ProductID, p.Name, p.ProductNumber, p.MakeFlag, p.FinishedGoodsFlag, p.Color, p.SafetyStockLevel," +
        "p.StandardCost, p.ListPrice, p.Size, p.SizeUnitMeasureCode, p.WeightUnitMeasureCode, p.Weight," +
        "REPLACE(p.ProductSubcategoryID, p.ProductSubcategoryID, pc.Name) as ProductSubcategoryID," +
        "REPLACE(p.ProductModelID, p.ProductModelID, pm.Name) as ProductModelID " +
        "FROM product p LEFT JOIN productsubcategory pc on p.ProductSubcategoryID = pc.ProductSubcategoryID " +
        "LEFT JOIN productmodel pm on p.ProductModelID = pm.ProductModelID ORDER BY p.ProductID DESC";

    db.query(query, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }

        result(null, res);
    });
};

module.exports = Product;
