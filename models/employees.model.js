const db = require("../config/database");

const Employee = function (employee) {
    this.EmployeeID = employee.EmployeeID;
    this.NationalIDNumber = employee.NationalIDNumber;
    this.LoginID = employee.LoginID;
    this.Title = employee.Title;
    this.BirthDate = employee.BirthDate;
    this.MaritalStatus = employee.MaritalStatus;
    this.Gender = employee.Gender;
    this.HireDate = employee.HireDate;
    this.SalariedFlag = employee.SalariedFlag;
    this.VacationHours = employee.VacationHours;
    this.StickLeaveHours = employee.StickLeaveHours;
    this.CurrentFlag = employee.CurrentFlag;
    this.rowguid = employee.rowguid;
    this.ModifiedDate = employee.ModifiedDate;
}

Employee.findAll = (results) => {
    let query = "SELECT e.EmployeeID, e.NationalIDNumber, REPLACE(e.ContactID, e.ContactID, concat(c.FirstName, ' ', c.LastName)) as ContactID," +
        " e.LoginID, e.ManagerID, e.Title, e.BirthDate, e.MaritalStatus, e.Gender, e.HireDate, e.SalariedFlag, " +
        "e.VacationHours, e.SickLeaveHours, e.CurrentFlag, e.ModifiedDate FROM employee e INNER JOIN contact c ON e.ContactID = c.ContactID;";

    db.query(query, (err, res) => {
        if (err) {
            results(null, err);
            return;
        }

        results(null, res);
    })
}

module.exports = Employee;
