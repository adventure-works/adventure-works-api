const db = require('../config/database');

const ProductSubcategory = function (productSubcategory) {
    this.ProductCategoryID = productSubcategory.ProductCategoryID;
    this.Name = productSubcategory.Name;
    this.rowguid = productSubcategory.rowguid;
    this.ModifiedDate = productSubcategory.ModifiedDate;
}

ProductSubcategory.findSubcategories = result => {
    let query = "SELECT * FROM productsubcategory";

    db.query(query, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }

        result(null, res);
    })
}

module.exports = ProductSubcategory;
