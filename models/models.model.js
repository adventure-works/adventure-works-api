const db = require('../config/database');

const ProductModel = function (productModel) {
    this.Name = productModel.Name;
    this.CatalogDescription = productModel.CatalogDescription;
    this.Instructions = productModel.Instructions;
    this.rowguid = productModel.rowguid;
    this.ModifiedDate = productModel.ModifiedDate;
}

ProductModel.findModels = result => {
    let query = "SELECT * FROM productmodel";

    db.query(query, (err, res) => {
        if (err) {
            result(null, err);
            return;
        }

        result(null, res);
    })
}

module.exports = ProductModel;
