/**
 * Dépendances
 */
const express = require('express');
const api = express();
const port = 3000;
const cors = require('cors');
const swaggerUI = require('swagger-ui-express');
const swaggerDocument = require('./config/swagger.json');

let corsParams = {
    origin: "http://localhost:4200"
}

/**
 * Express
 */
api.use(cors(corsParams));
api.use(express.json());
api.use(
  express.urlencoded({
    extended: true,
  }),
);

/**
 * Routing
 */
require("./routes/employees.routing")(api);
require("./routes/products.routing")(api);
require("./routes/subcategories.routing")(api);
require("./routes/models.routing")(api);
api.use("/api-docs", swaggerUI.serve,
    swaggerUI.setup(swaggerDocument));

api.listen(port, () => {
  console.log(`Serveur utilisé sur le port ${port}`);
});
