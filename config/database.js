const mysql = require("mysql2");

/**
 * Connexion Mysql
 */
const database = mysql.createConnection({
  host: "database",
  database: "adventureworks",
  user: "adventureuser",
  password: "password",
  port: "3306",
  insecureAuth: true
});

database.connect((err) => {
  if (err) throw err;
  console.log('Connecté au conteneur Docker MySQL !');
});

module.exports = database;
