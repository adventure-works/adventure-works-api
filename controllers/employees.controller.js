const Employees = require("../models/employees.model");

// Récupère tous les employées
exports.findAll = (req, res) => {
    Employees.findAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Une erreur s'est produite lors de la récupération des employées."
            });
        else res.status(200).send(data);
    });
};
