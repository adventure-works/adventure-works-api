const Subcategories = require("../models/subcategories.model");

exports.findSubcategories = (req, res) => {
    Subcategories.findSubcategories((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving products."
            });
        else res.status(200).send(data);
    });
};
