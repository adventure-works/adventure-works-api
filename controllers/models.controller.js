const Models = require("../models/models.model");

exports.findModels = (req, res) => {
    Models.findModels((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving products."
            });
        else res.status(200).send(data);
    });
};
