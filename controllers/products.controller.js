const Product = require("../models/products.model");

// Création d'un product
exports.create = (req, res) => {
    if (!req.body) {
        res.status(400).send({
            message: "Le contenu ne peut pas être vide !"
        });
    }
    const product = new Product({
        Name : req.body.Name,
        ProductNumber : req.body.ProductNumber,
        MakeFlag : req.body.MakeFlag,
        FinishedGoodsFlag : req.body.FinishedGoodsFlag,
        Color : req.body.Color,
        SafetyStockLevel : req.body.SafetyStockLevel,
        ReorderPoint : req.body.ReorderPoint,
        StandardCost : req.body.StandardCost,
        ListPrice : req.body.ListPrice,
        Size : req.body.Size,
        SizeUnitMeasureCode : req.body.SizeUnitMeasureCode,
        WeightUnitMeasureCode : req.body.WeightUnitMeasureCode,
        Weight : req.body.Weight,
        DaysToManufacture : req.body.DaysToManufacture,
        ProductLine : req.body.ProductLine,
        Class : req.body.Class,
        Style : req.body.Style,
        ProductSubcategoryID : req.body.ProductSubcategoryID,
        ProductModelID : req.body.ProductModelID,
        SellStartDate : req.body.SellStartDate,
        SellEndDate : req.body.SellEndDate,
        DiscontinuedDate : req.body.DiscontinuedDate,
        rowguid : req.body.rowguid,
        ModifiedDate : req.body.ModifiedDate,
    });

    Product.create(product, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Des erreurs sont survenus pendant la création d'un produit."
            });
        else res.status(200).send(data);
    });
};

exports.findAll = (req, res) => {
    Product.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving products."
            });
        else res.status(200).send(data);
    });
};

