module.exports = api => {
    const products = require("../controllers/products.controller");
    const router = require("express").Router();

    // Création d'un product
    router.post("/create", products.create);

    router.get("/", products.findAll);

    api.use('/products', router);
};


