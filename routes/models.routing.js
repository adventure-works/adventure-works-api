module.exports = api => {
    const models = require("../controllers/models.controller");
    const router = require("express").Router();

    router.get("/", models.findModels);

    api.use('/models', router);
};


