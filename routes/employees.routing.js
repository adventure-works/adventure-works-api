module.exports = api => {
    const employees = require("../controllers/employees.controller");
    const router = require("express").Router();

    // Récupération de tous les employées
    router.get("/", employees.findAll);

    api.use('/employees', router);
};


