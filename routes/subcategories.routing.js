module.exports = api => {
    const subcategories = require("../controllers/subcategories.controller");
    const router = require("express").Router();

    router.get("/", subcategories.findSubcategories);

    api.use('/subcategories', router);
};


