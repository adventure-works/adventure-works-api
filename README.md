# Projet rattrapage - AdventureWorks
## A propos du projet

Cette stack gère l'API afin de pouvoir requêter sur la base de données via le network.

### Getting Started
Voici les différentes étapes listées ci-dessous pour pouvoir obtenir une copie locale opérationnelle.

### Prérequis
* **[Docker-Compose](https://docs.docker.com/compose/install/)**

### Technologies utilisés
* **[NodeJS](https://nodejs.org/en/)**
* **[ExpressJS](https://expressjs.com/fr/)**

### Installation

#### Installation et lancement du conteneur via un *terminal*.

+ Clonage du projet
````bash
git clone https://gitlab.com/adventure-works/adventure-works-api.git
````

+ Se déplacer dans le projet
````bash
cd adventure-works-api
````

+ Lancement du conteneur

````bash
docker-compose up -d
````

:warning: Une erreur peut survenir lors du lancement sur la connexion avec le conteneur de base de données :warning:

Pour corriger le problème, lancer le terminal via les commandes suivantes:

```bash
docker-compose run node sh 

# et enfin

npm install
```

### Utilisation

* Afin de pouvoir requêter la base de données, un lien pour accéder à la documentation technique de l'API est disponible ici **[http://localhost:3000/api-docs](http://localhost:3000/api-docs)**.
